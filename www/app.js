(function() {'use strict';

	var module = angular.module('myApp', ['onsen', 'base64', 'hljs']);

	module.controller('DetailController', function($scope, exploits, $base64) {
		hljs.initHighlightingOnLoad();
		$scope.exploit = exploits.selectedItem;
		console.log("DC item", $scope.exploit);
		$scope.exploit.code = $base64.decode($scope.exploit.b64);
	});

	module.controller('MasterController', function($scope, exploits, $http) {
		$scope.items = {};
		$scope.groups = {};
		$scope.filesArray = [];
		$scope.isLoadingData = true;
		//modalLoad.show();
		exploits.getExploits().then(function(response) {	
			$scope.items = response.exploits;
			$scope.groups = _.groupBy($scope.items, "platform");
			$scope.isShow = false;
            $scope.$apply();
			$scope.isLoadingData = false;
			//console.log($scope.groups);
		}, function(error) {
			console.error(error);
		});
		//modalLoad.hide();

		
		$scope.showDetail = function(platform, index) {
			console.log("exploits", $scope.groups);
			var sGroup = $scope.groups[platform];
			console.log("group", platform, sGroup);
			var selectedItem = sGroup[index];
			console.log("item", selectedItem);
			exploits.selectedItem = selectedItem;
			$scope.ons.navigator.pushPage('detail.html', {
				title : selectedItem.title
			});
		};

		$scope.showHelp = function() {
			//angular.forEach($scope.filesArray, function(key, value) {

			//});
			$scope.ons.navigator.pushPage('help.html', {
				title : 'About'
			});
		};
		
		$scope.showLoading = function() {
			$scope.ons.navigator.pushPage('loading.html', {
				title : 'Loading'
			});
		};

		$scope.bundleFiles = function() {
			var fileList = _.compact($scope.filesArray);
			if (fileList.length == 0) {
				//alert("Please select at least one item to bundle");
				console.log("default all items selected", $scope.items);
				fileList = $scope.items;
				//return;
			}
			console.log("files: ", fileList);
			var zip = new JSZip();
			console.log("zip: ", zip);

			var indexText = "";
			angular.forEach(fileList, function(key, value) {
				console.log(key.file.replace(/^.*[\\\/]/, ''));
				zip.file(key.platform + '/' + key.type + '/' + key.file.replace(/^.*[\\\/]/, ''), key.b64, {
					base64 : true,
					createFolders : true
				});
				indexText += key.platform + '/' + key.type + '/' + key.file.replace(/^.*[\\\/]/, '') + " - " + key.description + "\n";
			});

			zip.file("00_Index.txt", indexText);
			var content = zip.generate({
				type : "blob"
			});

			var d = new Date();
			var fname = "exploits-" + d.toString('yyyyMMddHHmmss') + ".zip";

			window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
			function gotFS(fileSystem) {
				var d = new Date();
				console.log("------DATE------", d);
				fileSystem.root.getDirectory("LaPenetrada", {
					create : true
				}, gotDir);
			}

			function gotDir(dirEntry) {
				dirEntry.getFile(fname, {
					create : true,
					exclusive : false
				}, gotFileEntry, fail);
			}

			function gotFileEntry(fileEntry) {
				fileEntry.createWriter(gotFileWriter, fail);
			}

			function gotFileWriter(writer) {
				//var binContent = Base64.decode(content);
				console.log(content);
				console.log("================================");
				console.log(_.pairs(writer));
				//console.log(binContent);
				var b64File = zip.generate({
					type : "base64"
				});
				writer.write(content);
				window.resolveLocalFileSystemURL(writer.localURL, function(err) {
					writer.onwriteend = function(evt) {
						//alert("ZIP File Written....", evt);
						//alert("File saved to: " + fname);
						window.plugins.socialsharing.shareViaEmail(indexText, 'SecMate - Exploit List ' + fname, null, // TO: must be null or an array
						null, // CC: must be null or an array
						null, // BCC: must be null or an array
						[err.nativeURL],
						//['data:application/zip;base64,' + b64File], // FILES: can be null, a string, or an array
						fail, // called when sharing worked, but also when the user cancelled sharing via email (I've found no way to detect the difference)
						fail // called when sh*t hits the fan
						);
					};
				});
				//writer.write("some sample text");
			}

			function fail(error) {
				console.log(error.code);
			}

		};

		$scope.toggleSelect = function(e, a) {
			console.log(a);
			if (!a) {
				//$scope.filesArray.splice(e.fIndex, 1);
				$scope.filesArray[e.fIndex - 1] = null;
			} else {
				e.fIndex = ($scope.filesArray.push(e));
			}
			//console.log("files: ",$scope.filesArray,_.compact($scope.filesArray));
		};

	});

	module.factory('exploits', function($http, $q) {
		//$scope.showLoading();
		//modal.show();
		return {
			selectedItem : {},
			getExploits : function() {
				var deferred = $q.defer();
				//modal.show();
				var url = "http://penetrada.appmofo.com/api?callback=JSON_CALLBACK";
				$http.jsonp(url).success(function(data, status, headers, config) {
					//console.log(data);
					//this.items=data;
					//showLoading();
		
					deferred.resolve(data);
					
				}).error(function(data, status, headers, config) {
					//this always gets called
					console.log(status);
					deferred.reject(status);
				});
				//modal.hide();
				return deferred.promise;

			}
		};
	});

})();
